## Checar resultados das eleições 2022 no terminal em tempo real

```
while true; do sleep 5; curl https://resultados.tse.jus.br/oficial/ele2022/544/dados-simplificados/br/br-c0001-e000544-r.json 2> /dev/null | jq '.cand[] | select((.seq == "1") or select(.seq == "2"))'; done

```

## Checar resultado total

```
curl https://resultados.tse.jus.br/oficial/ele2022/544/dados-simplificados/br/br-c0001-e000544-r.json 2> /dev/null | jq '.cand[] | select((.seq >= "1"))';

```